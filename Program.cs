﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Console Clear is to remove noise
            Console.Clear();

            Console.WriteLine("Today is Wednesday the 8th of March 2017");
            Console.WriteLine("<Press any key to exit the App>");
            Console.ReadKey();

        }
    }
}
